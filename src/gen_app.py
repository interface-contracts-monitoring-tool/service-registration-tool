from flask import Flask, render_template, request, Response
from flask_oidc import OpenIDConnect
from werkzeug.routing import Rule
import requests
import logging
from sqlitedict import SqliteDict
import json

from openapi_spec_validator import validate_v3_spec
import argparse

logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

parser = argparse.ArgumentParser()
parser.add_argument("--mount_point", help="Mount point for the SRT webapp instance", type=str)
args = parser.parse_args()


def getServiceRegistry(SRUrl:str, token:str):
    try:
        r = requests.get(
            url=SRUrl,
            headers={
                "Authorization": f"Bearer {token}",
                "Content-Type": "application/json"
            }
        )
        if r.ok:
            return r.json()
        else:
            return None
    except Exception as e:
        logger.error(e)
        return None

def getService(service_id: str, token: str):
    service_registry = getServiceRegistry("https://efpf.smecluster.com/apis/sr/", token)
    for service in service_registry["services"]:
        if service["id"] == service_id:
            return service
    return None


app = Flask(__name__, static_url_path=f"/{args.mount_point}")
app.url_rule_class = lambda path, **options: Rule(f"/{args.mount_point}" + path, **options)
app.config.update({
    'SECRET_KEY': 'SomethingNotEntirelySecret',
    'TESTING': True,
    'DEBUG': True,
    'OIDC_CLIENT_SECRETS': 'data/client_secrets.json',
    'OIDC_ID_TOKEN_COOKIE_SECURE': False,
    'OIDC_REQUIRE_VERIFIED_EMAIL': False,
    'OIDC_USER_INFO_ENABLED': True,
    'OIDC_OPENID_REALM': 'efpf',
    'OIDC_SCOPES': ['openid', 'email', 'profile'],
    'OIDC_INTROSPECTION_AUTH_METHOD': 'client_secret_post',
    'OIDC_CALLBACK_ROUTE': '/custom_callback'
    #'OVERWRITE_REDIRECT_URI': 'https://efpf.polito.it/s/oidc_callback'
})



oidc = OpenIDConnect(app, credentials_store=SqliteDict('data/credentials.db', autocommit=True))

@app.route('/custom_callback')
@oidc.custom_callback
def callback(data):
    logger.info(data)
    return 'Hello. You submitted %s' % data

@app.route('/')
@oidc.require_login
def index():
    return render_template("index.html", token=oidc.get_access_token())

@app.route('/new')
@oidc.require_login
def newService():
    return render_template("service.html", load="no", loadedService=None)

@app.route('/load')
@oidc.require_login
def loadService():
    token = oidc.get_access_token()

    service_id = request.args.get('service_id', type = str)
    service = getService(service_id, token)

    try:
        for i, api in enumerate(service["apis"]):
            if "application/vnd.oai.openapi+json" in api["spec"]["mediaType"]:
                service["apis"][i]["specMediaTypeSpec"] = "OpenAPI"
                service["apis"][i]["specMediaTypeFormat"] = "JSON"
                service["apis"][i]["specSchema"] = api["spec"]["schema"]
                service["apis"][i]["specUrl"] = api["spec"]["url"]
    except:
        logger.error("Failure in loading specs details")

    return render_template("service.html", load="yes", loadedService=service, token=oidc.get_access_token())

@app.route('/validate/api', methods=['POST'])
@oidc.require_login
def validateApi():
    # TODO
    # Use https://github.com/p1c2u/openapi-core and
    # https://pypi.org/project/asyncapi/ to validate the schemas

    api = request.get_json()
    try:
        if api["specMediaTypeSpec"] == "OpenAPI":
            spec = json.loads(api["specSchema"])
            logger.info(spec)
            validate_v3_spec(spec)
            return json.dumps({"valid": True})
        elif api["specMediaTypeSpec"] == "AsyncAPI":
            return json.dumps({"valid": True})
        else:
            return json.dumps({"valid": True})
    except Exception as e:
        logger.error(e)
        return json.dumps({"valid": False})

@app.route('/validate/service', methods=['POST'])
@oidc.require_login
def validateService():

    service = request.get_json()
    try:
        if ( service["id"] == "" or service["title"] == "" or service["type"] == "" or
            service["doc"] == "" ):
            return json.dumps({"valid": False})
        return json.dumps({"valid": True})
    except Exception as e:
        logger.error(e)
        return json.dumps({"valid": False})


@app.route('/submit/service', methods=['POST'])
@oidc.require_login
def submitService():
    time_intervals = {
        "1 hour": 60*60,
        "1 day": 60*60*24,
        "1 week": 60*60*24*7,
        "1 month": 60*60*24*30,
        "1 year": 60*60*24*365
    }
    
    service = request.get_json()
    
    service["ttl"] = time_intervals[service["ttl"]]
    

    logger.info(service)
    
    for i, api in enumerate(service["apis"]):
        
        service["apis"][i]["spec"] = {}
        if service["apis"][i]["specMediaTypeSpec"] == "OpenAPI":
            if service["apis"][i]["specMediaTypeFormat"] == "JSON":
                service["apis"][i]["spec"]["mediaType"] = "application/vnd.oai.openapi;version=3.0"
            elif service["apis"][i]["specMediaTypeFormat"] == "YAML":
                service["apis"][i]["spec"]["mediaType"] = "application/vnd.oai.openapi+json;version=3.0"
        elif service["apis"][i]["specMediaTypeSpec"] == "AsyncAPI":
            if service["apis"][i]["specMediaTypeFormat"] == "JSON":
                service["apis"][i]["spec"]["mediaType"] = "application/vnd.aai.asyncapi;version=2.0.0"
            elif service["apis"][i]["specMediaTypeFormat"] == "YAML":
                service["apis"][i]["spec"]["mediaType"] = "application/vnd.aai.asyncapi+json;version=2.0.0"
        
        del service["apis"][i]["specMediaTypeSpec"]
        del service["apis"][i]["specMediaTypeFormat"]
        
        if service["apis"][i]["specUrl"] == "":
            service["apis"][i]["spec"]["url"] = None
        else:
            service["apis"][i]["spec"]["url"] = service["apis"][i]["specUrl"]
            
        if service["apis"][i]["specSchema"] == "":
            service["apis"][i]["spec"]["schema"] = None
        else:
            service["apis"][i]["spec"]["schema"] = service["apis"][i]["specSchema"]
        
        del service["apis"][i]["specUrl"]
        del service["apis"][i]["specSchema"]
        
        if type(service["apis"][i]["meta"]) != dict:
            if service["apis"][i]["meta"] == "":
                service["apis"][i]["meta"] = {}
            else:
                service["apis"][i]["meta"] = json.loads(service["apis"][i]["meta"].encode('utf-8').decode('utf-8'))
        
    logger.info(json.dumps(service))
    # try:
    #     if api["specMediaTypeSpec"] == "OpenAPI":
    #         spec = json.loads(api["specSchema"])
    #         print(spec)
    #         validate_v3_spec(spec)
    #         return json.dumps({"valid": True})
    #     elif api["specMediaTypeSpec"] == "AsyncAPI":
    #         return json.dumps({"valid": True})
    # except Exception as e:
    #     print(e)
    #     return json.dumps({"valid": False})
    
    return json.dumps({"valid": True})

@app.route('/service/exists', methods=['GET'])
@oidc.require_login
def serviceExists():
    token = oidc.get_access_token()
    service_id = request.args.get('service_id', type = str)
    service = getService(service_id, token)
    if service is not None:
        return Response(status=200)
    else:
        return Response(status=404)


@app.route('/delete')
@oidc.require_login
def deleteService():
    token = oidc.get_access_token()
    service_id = request.args.get('service_id', type = str)
    service = getService(service_id, token)
    return service