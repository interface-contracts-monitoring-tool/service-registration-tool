window.addEventListener("DOMContentLoaded", function () {

    var buttonNew = document.getElementById("buttonNew");
    buttonNew.addEventListener("click", function () {
        location.href = "/srt/new";
    });

    var buttonLoad = document.getElementById("buttonLoad");
    buttonLoad.addEventListener("click", async function () {
        var service_id = document.forms['formLoad']['service_id_load'].value;
        let response = await fetch('/srt/service/exists?' + new URLSearchParams({
            service_id: service_id
        }), {
            method: "GET",
        }).catch(err => {
            // if any error occured, then catch it here
            console.error(err);
        });
        if (response.status == 200) {
            location.href = "/srt/load?service_id=" + service_id;
        } else {
            alert("Requested service does not exist.");
        }
    });

    var buttonDelete = document.getElementById("buttonDelete");
    buttonDelete.addEventListener("click", async function () {
        var service_id = document.forms['formLoad']['service_id_load'].value;
        let response = await fetch('/srt/service/exists?' + new URLSearchParams({
            service_id: service_id
        }), {
            method: "GET",
        }).catch(err => {
            // if any error occured, then catch it here
            console.error(err);
        });
        if (response.status == 200) {
            location.href = "/srt/delete?service_id=" + service_id;
        } else {
            alert("Requested service does not exist.");
        }
    });
});