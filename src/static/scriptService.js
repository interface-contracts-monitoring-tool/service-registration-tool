var service = new Object();
var apis = [];

// ---------------------------------------- Data Models --------------------------------------

function createService() {
    service.id = document.getElementById("inputServiceID").value;
    service.title = document.getElementById("inputServiceTitle").value;
    service.description = document.getElementById("inputServiceDescription").value;
    service.type = document.getElementById("inputServiceType").value;
    service.doc = document.getElementById("inputServiceDocURL").value;
    service.ttl = document.getElementById("inputServiceExpiration").value;
    service.meta = {}
    service.apis = apis;

    return service;
}

function createAPI() {
    var api = new Object();
    
    api.id = document.getElementById("inputAPIID").value;
    api.title = document.getElementById("inputAPITitle").value;
    api.description = document.getElementById("inputAPIDescription").value;
    api.protocol = document.getElementById("inputAPIProtocol").value;
    api.url = document.getElementById("inputAPIURL").value;
    api.specMediaTypeSpec = document.getElementById("inputAPISpecType").value;
    api.specMediaTypeFormat = document.getElementById("inputAPISpecFormat").value;
    api.specSchema = document.getElementById("inputAPISpec").value;
    api.specUrl = document.getElementById("inputAPIDocURL").value;
    api.meta = document.getElementById("inputAPIMeta").value;

    return api;
}


// ---------------------------------------- Functions --------------------------------------


function addAPITableRow(apiID, apiTitle, apiDescription) {
    var table = document.getElementById("table-api-list-body");
    // Create an empty <tr> element and add it to the 1st position of the table:
    var row = table.insertRow(-1);
    row.id = 'row-api-' + apiID;

    // Insert new cells (<td> elements) at the 1st and 2nd position of the "new" <tr> element:
    var cell1 = row.insertCell(0);
    var cell2 = row.insertCell(1);
    var cell3 = row.insertCell(2);
    var cell4 = row.insertCell(3);

    // Add some text to the new cells:
    cell1.innerHTML = apiTitle;
    cell2.innerHTML = apiDescription;
    cell3.innerHTML = '<button type="button" id="btn-edit-api-' + apiID + '" class="btn btn-primary btn-square-md" onClick="editAPIRow(this.id)">Edit</button>';
    cell4.innerHTML = '<button type="button" id="btn-delete-api-' + apiID + '" class="btn btn-danger btn-square-md" onClick="deleteAPIRow(this.id)">Delete</button>';
}

function loadService(loadedService) {
    service = JSON.parse(loadedService);
    
    delete service.updatedAt;
    delete service.expiresAt;
    delete service.createdAt;
    
    console.log(service);
    document.getElementById("inputServiceID").value = service.id;
    document.getElementById("inputServiceTitle").value = service.title;
    document.getElementById("inputServiceDescription").value = service.description;
    document.getElementById("inputServiceType").value = service.type;
    document.getElementById("inputServiceDocURL").value = service.doc;

    document.getElementById("api-list-container").style.visibility = "visible";

    for (var i=0; i<service.apis.length; i++){
        console.log(service.apis[i]);
        addAPITableRow(service.apis[i].id, service.apis[i].title, service.apis[i].description);
    }

    apis = service.apis

}


function deleteAPIRow(button_id) {
    var api_id = button_id.replace('btn-delete-api-', '');
    var table = document.getElementById("table-api-list-body");
    
    for (var i = 0, row; row = table.rows[i]; i++) {
        if (row.id ==  'row-api-' + api_id) {
            console.log(row.id)
            table.deleteRow(i);
            break;
        }
    }

    for (var i = 0; i < apis.length; i++){
        if (apis[i].id == api_id) {
            apis.pop(i);
            break
        }
    }
}

function editAPIRow(button_id) {
    var api_id = button_id.replace('btn-edit-api-', '');
    console.log(api_id);
    var table = document.getElementById("table-api-list-body");
    for (var i = 0, row; row = table.rows[i]; i++) {
        if (row.id ==  'row-api-' + api_id) {
            console.log(row.id)
            table.deleteRow(i);
            break;
        }
    }

    for (var i = 0; i < apis.length; i++){
        if (apis[i].id == api_id) {
            console.log(apis[i].id)
            var api = apis.splice(i,1)[0];
            break
        }
    }

    console.log(api)
    document.getElementById("inputAPIID").value = api.id;
    document.getElementById("inputAPITitle").value = api.title;
    document.getElementById("inputAPIDescription").value = api.description;
    document.getElementById("inputAPIProtocol").value = api.protocol;
    document.getElementById("inputAPIURL").value = api.url;
    document.getElementById("inputAPISpecType").value = api.specMediaTypeSpec;
    document.getElementById("inputAPISpecFormat").value = api.specMediaTypeFormat;
    document.getElementById("inputAPISpec").value = api.specSchema;
    document.getElementById("inputAPIDocURL").value = api.specUrl;
    
    $('#modalAddAPI').modal('show');

}

window.addEventListener("DOMContentLoaded", function () {

    var formAPI = document.forms.formAPI;
    var formService = document.forms.formService;

    var table = document.getElementById("table-api-list-body");

    document.getElementById("buttonCancelAPI").addEventListener("click", function () {
        formAPI.reset();
        $('#modalAddAPI').modal('hide');
    });

    document.getElementById("buttonSubmitAPI").addEventListener("click", async function () {

        var api = createAPI();

        const myHeaders = new Headers();
        myHeaders.append('Content-Type', 'application/json');
        let response = await fetch('/srt/validate/api' , {
            method:"POST",
            headers: myHeaders,
            body: JSON.stringify(api),

        }).catch(err => {
            // if any error occured, then catch it here
            console.error(err);
        });

        console.log(response);

        if (response.ok) {

            var resp = await response.json();

            console.log(resp.valid);

            if (resp.valid) {
                apis.push(api);
                addAPITableRow(api.id, api.title, api.description);
    
                formAPI.reset();
                document.getElementById("api-list-container").style.visibility = "visible";
                $('#modalAddAPI').modal('hide');
                
                console.log("Created API: ", api);
            } else {
                alert("Invalid API definition");
            }
        }
    });

    document.getElementById("buttonSubmitService").addEventListener("click", async function () {
        $('#modalSubmitting').modal({backdrop: 'static', keyboard: false});
        $('#modalSubmitting').modal('toggle');

        createService();

        console.log(JSON.stringify(service))

        const myHeaders = new Headers();
        myHeaders.append('Content-Type', 'application/json');
        let response = await fetch('/srt/submit/service' , {
            method:"POST",
            headers: myHeaders,
            body: JSON.stringify(service),

        }).catch(err => {
            // if any error occured, then catch it here
            console.error(err);
        });

        console.log(response);

        if (response.ok) {

            var resp = await response.json();

            console.log(resp.valid);

            if (resp.valid) {
                formService.reset();
                document.getElementById("api-list-container").style.visibility = "hidden";
                table.innerHTML = '';
                $('#modalSubmitting').modal('hide');
                window.location.href = "/srt";
                console.log("Submitted service")
            } else {
                $('#modalSubmitting').modal('hide');
                alert("Invalid Service definition");
            }
        }
        
    });

});