from src.gen_app import app
import os

if __name__ == "__main__":
    app.run(debug=True, host="0.0.0.0", port=int(os.getenv("MOUNT_PORT")))